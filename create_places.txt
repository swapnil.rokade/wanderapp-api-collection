Url : https://dev-wanderapp.pantheonsite.io/create_places.json
Method : POST
Request Params :
    {
	    "place_name" : "Test Place 7",
	    "place_id" : "asasasawwe@dmin123",
  		"latitude":"123456789523",
  		"longitude":"1236547961",
  		"opening_hours":true,
  		"rating":"4.6",
  		"vicinity":"This is my etsasa address",
  		"type":[
        	"accounting","airport","atm"
        ]
    }
Response :

{
    "data": {
        "place_name": "Test Place 7",
        "place_id": "asasasawwe@dmin123"
    },
    "status": true,
    "message": "Places created successfully."
} 