Url: https://dev-wanderapp.pantheonsite.io/user_check_email_pwd.json
Method: POST 
Success Request Params:
    {
        "mail" : "abhi@web.com",
        "password": "Pass@123"
    }

Success Response:
    {
    "status": true,
    "data": {
        "user_role": "customer",
        "uid": "73"
    },
    "message": "User exist with this email id and password"
    }

Error Request Params:
    {
        "mail" : "abhi@web.com",
        "password": "Pass@1231"
    }  

    OR

    {
        "mail" : "abhi@web.com111",
        "password": "Pass@1231"
    }
Error Response:
    {
        "status": false,
        "message": "Password does not match."
    }    

    OR

    {
        "status": false,
        "message": "User does not exist with this email id"
    }

================================================================================================================


Url: https://dev-wanderapp.pantheonsite.io/user_check_email.json
Method: POST 
Success Request Params:
    {
        "mail" : "abhi@web.com"
    }

Success Response:
    {
    "status": true,
    "data": {
        "user_role": "customer",
        "uid": "73"
    },
    "message": "User exist with this email id"
    }

Error Request Params:
    {
        "mail" : "abhi@web.com111"
    }  

Error Response:
    {
        "status": false,
        "message": "User does not exist with this email id"
    }

==========================================================================================================

https://dev-wanderapp.pantheonsite.io//business-recommendation-typewise/all

https://dev-wanderapp.pantheonsite.io//business-recommendation-typewise/25 (taxonomy id of Place Type)

